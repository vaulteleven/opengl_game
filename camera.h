#ifndef CAMERA_H
#define CAMERA_H

#include <GL/glew.h>
#include <glm/glm.hpp>

class Camera
{
    public:
        // camera position vectors
        glm::vec3 cam_pos;
        glm::vec3 cam_front;
        glm::vec3 cam_up;

        // camera mouse constants
        GLfloat cam_yaw; 
        GLfloat cam_pitch; 
        GLfloat cam_speed; 
        GLfloat cam_sens; 

        // constructor
        Camera(glm::vec3, glm::vec3, glm::vec3, GLfloat, GLfloat, GLfloat, GLfloat);

        // enum for 4 different camera movements
        enum CameraMovement{FORWARD, BACKWARD, LEFT, RIGHT};

        // get view mat
        glm::mat4 getViewMat();

        // handle keyboard and mouse input for camera movement
        void handleKeyboardMovement(CameraMovement, GLfloat delta_time);
        void handleMouseMovement(GLfloat x_offset, GLfloat y_offset);

    private:
        // update the cameras vectors
        void updateCameraVectors();
};
#endif
