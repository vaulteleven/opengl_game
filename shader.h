#ifndef SHADER_H
#define SHADER_H

#include<GL/glew.h>
#include<glm/glm.hpp>

#include<string>

class Shader
{
    public:
        Shader(const GLchar*, const GLchar*, const GLchar*);
        void use();
        void setBool(const std::string&, GLboolean) const;
        void setInt(const std::string&, GLint) const;
        void setFloat(const std::string&, GLfloat) const;
        void setVec2(const std::string&, const glm::vec2&) const;
        void setVec2(const std::string&, GLfloat, GLfloat) const;
        void setVec3(const std::string&, const glm::vec3&) const;
        void setVec3(const std::string&, GLfloat, GLfloat, GLfloat) const;
        void setVec4(const std::string&, const glm::vec4&) const;
        void setVec4(const std::string&, GLfloat, GLfloat, GLfloat, GLfloat);
        void setMat2(const std::string&, const glm::mat2&) const;
        void setMat3(const std::string&, const glm::mat3&) const;
        void setMat4(const std::string&, const glm::mat4&) const;

        GLuint getID();
        void setID(GLuint);

    private:
        void checkCompileErrors(GLuint, std::string);
        GLuint ID;
};
#endif
