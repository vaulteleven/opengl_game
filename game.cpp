#include <SDL2/SDL.h>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glx.h>

#include <SOIL/SOIL.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "shader.h"
#include "camera.h"


// screen constants
const GLint SCREEN_WIDTH = 1280;
const GLint SCREEN_HEIGHT = 720;

SDL_Window* glWindow = NULL;
SDL_GLContext glContext = NULL;
SDL_Surface* screenSurface = NULL;
SDL_Surface* imageSurface = NULL;

glm::vec3 lamp_pos = glm::vec3(1.2f, 1.0f, 2.0f);

GLfloat delta_time = 0.0f;
GLfloat last_frame = 0.0f;

GLfloat last_x = SCREEN_WIDTH / 2;
GLfloat last_y = SCREEN_HEIGHT / 2;

GLfloat fov = 45.0f;

GLfloat vertices[] = {
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,
    0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f, 
    0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f, 
    0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f, 
    -0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f, 
    -0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f, 

    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
    0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
    0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
    0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,
    -0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,

    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,
    -0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,

    0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
    0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
    0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
    0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,
    0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,
    0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,

    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
    0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,
    0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
    0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
    -0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,
    -0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,

    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
    0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,
    0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
    0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
    -0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,
    -0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f
};

void 
initGL()
{
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
}

void 
initGLEW()
{
    glewExperimental = GL_TRUE;

    if(glewInit() != GLEW_OK)
    {
        printf("failed to init glew");
    }
}

GLint 
initWindow()
{
    GLint success = 1;

    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0)
    {
        printf("\tSDL could not initialize: SDL_Error: %s\n", SDL_GetError());
        success = 0;
    } 
    else 
    {
        glWindow = SDL_CreateWindow("OGL Game", 
                SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                SCREEN_WIDTH,
                SCREEN_HEIGHT,
                SDL_WINDOW_OPENGL);

        if(glWindow == NULL)
        {
            printf("Window could not be created\n SDL_Error: %s", SDL_GetError());
            success = 0;
        }
        else
        {
            initGL();

            glContext = SDL_GL_CreateContext(glWindow);

            SDL_GL_SetSwapInterval(1);

            initGLEW();

            glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

            SDL_SetRelativeMouseMode(SDL_TRUE);

            glEnable(GL_DEPTH_TEST);
        }
    }

    return success;
}

void 
cleanup()
{
    SDL_FreeSurface(imageSurface);
    imageSurface = NULL;

    SDL_GL_DeleteContext(glContext);
    glContext = NULL;

    SDL_DestroyWindow(glWindow);
    glWindow = NULL;

    SDL_Quit();
}

GLvoid
handle_mouse_input(Camera *camera)
{
    SDL_Event event;

    while(SDL_PollEvent(&event))
    {
        if(event.type == SDL_MOUSEMOTION)
        {
            GLfloat x_offset = event.motion.xrel;
            GLfloat y_offset = event.motion.yrel;

            camera->handleMouseMovement(x_offset, y_offset);

        }
    }
}

GLuint
handle_keyboard_input(Camera *camera)
{
    SDL_Event event;

    GLfloat current_frame = SDL_GetTicks() / 1000.0f;
    delta_time = current_frame - last_frame;
    last_frame = current_frame;

    GLuint game_running = 1;

    const Uint8 *keys = SDL_GetKeyboardState(NULL);

    if(keys[SDL_SCANCODE_ESCAPE])
    {
        printf("key pressed: ESC\n");
        game_running = 0;
    }

    if(keys[SDL_SCANCODE_L])
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    if(keys[SDL_SCANCODE_W])
        camera->handleKeyboardMovement(camera->FORWARD, delta_time);
    if(keys[SDL_SCANCODE_S])
        camera->handleKeyboardMovement(camera->BACKWARD, delta_time);
    if(keys[SDL_SCANCODE_A])
        camera->handleKeyboardMovement(camera->LEFT, delta_time);
    if(keys[SDL_SCANCODE_D])
        camera->handleKeyboardMovement(camera->RIGHT, delta_time);

    return game_running;
}

GLuint 
generate_texture(GLchar *texture_source)
{
    GLuint texture;
    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);

    // set texture wrapping params
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // set texture filtering params
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    int width, height;
    unsigned char* image = SOIL_load_image(texture_source, &width, &height, 0, 
            SOIL_LOAD_RGB);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, 
            image);
    glGenerateMipmap(GL_TEXTURE_2D);
    SOIL_free_image_data(image);
    glBindTexture(GL_TEXTURE_2D, 0);

    return texture;
}

int 
main(int argc, char* argv[])
{
    if(!initWindow())
    {
        printf("\tfailed to init window"); 
    }

    Shader containerShader("shaders/v_shader.glsl", "shaders/f_shader.glsl", NULL);
    Shader lampShader("shaders/lamp_v_shader.glsl", "shaders/lamp_f_shader.glsl", NULL);

    Camera camera(glm::vec3(0.0f, 0.0f, 3.0f),
                  glm::vec3(0.0f, 0.0f, -1.0f),
                  glm::vec3(0.0f, 1.0f, 0.0f),
                  -90.0f, 0.0f, 5.0f, 0.04f);

    GLuint VBO, containerVAO, lampVAO;

    glGenVertexArrays(1, &containerVAO);
    glGenBuffers(1, &VBO);
    glBindVertexArray(containerVAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), 
            (GLvoid*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), 
            (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    glGenVertexArrays(1, &lampVAO);
    glBindVertexArray(lampVAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);
    glBindVertexArray(0);

    GLboolean game_running = 1;

    //    GLuint texture;
    //    texture = generate_texture("textures/radioactive.jpg");

    GLuint timer;
    GLfloat aspect = (GLfloat)SCREEN_WIDTH / (GLfloat)SCREEN_HEIGHT;

    containerShader.use();

    // game loop | drawing loop
    while(game_running)
    {
        timer = SDL_GetTicks();
        GLfloat size_value = (sin(timer/1000.0f));

        game_running = handle_keyboard_input(&camera);
        handle_mouse_input(&camera);

        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        containerShader.use();

        glm::mat4 proj_mat = 
            glm::perspective(glm::radians(fov), aspect, 0.1f, 100.0f);
        containerShader.setMat4("projection", proj_mat);

        glm::mat4 view_mat = camera.getViewMat();
        containerShader.setMat4("view", view_mat);

        glm::mat4 model_mat;
        containerShader.setMat4("model", model_mat);

        containerShader.setVec3("object_color", 0.2f, 0.2f, 1.0f);
        containerShader.setVec3("light_color", 1.0f, 1.0f, 1.0f);
        containerShader.setVec3("light_position", lamp_pos);
        containerShader.setVec3("camera_position", camera.cam_pos);

        //glBindTexture(GL_TEXTURE_2D, texture);

        glBindVertexArray(containerVAO);
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);

        // lamp vao and shader
        lampShader.use();
        model_mat = glm::translate(model_mat, lamp_pos);
        model_mat = glm::scale(model_mat, glm::vec3(0.2f, 0.2f, 0.2f));

        // floating lamp that floats around the cube
        GLfloat radius = 3.0f;
        GLfloat camX = sin(timer/2000.0f) * radius;
        GLfloat camZ = cos(timer/2000.0f) * radius;
        lamp_pos.x = camX;
        lamp_pos.y = 1.0f;
        lamp_pos.z = camZ;

        lampShader.setMat4("model", model_mat);
        lampShader.setMat4("view", view_mat);
        lampShader.setMat4("projection", proj_mat);

        glBindVertexArray(lampVAO); 
        glDrawArrays(GL_TRIANGLES, 0, 36);
        glBindVertexArray(0);

        SDL_GL_SwapWindow(glWindow);
    }

    glDeleteVertexArrays(1, &containerVAO);
    glDeleteBuffers(1, &VBO);

    cleanup();

    return 0;
}
