#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "camera.h"

#define PI180 M_PI/180

Camera::Camera(glm::vec3 pos, glm::vec3 front, glm::vec3 up,
               GLfloat yaw, GLfloat pitch, GLfloat speed, GLfloat sens)
{
    cam_pos = pos;
    cam_front = front;
    cam_up = up;

    cam_yaw = yaw;
    cam_pitch = pitch;
    cam_speed = speed;
    cam_sens = sens;
}

glm::mat4 Camera::getViewMat()
{
    return glm::lookAt(cam_pos, cam_pos + cam_front, cam_up);
}

void Camera::handleKeyboardMovement(CameraMovement movement, GLfloat d_time)
{
    GLfloat velocity  = Camera::cam_speed * d_time;
    if(movement == FORWARD)
        cam_pos += cam_front * velocity;
    if(movement == BACKWARD)
        cam_pos -= cam_front * velocity;
    if(movement == LEFT)
        cam_pos -= glm::normalize(glm::cross(cam_front, cam_up)) * velocity;
    if(movement == RIGHT)
        cam_pos += glm::normalize(glm::cross(cam_front, cam_up)) * velocity;
}

void Camera::handleMouseMovement(GLfloat x_offset, GLfloat y_offset)
{
    x_offset *= cam_sens;
    y_offset *= cam_sens;

    cam_yaw += x_offset;
    cam_pitch -= y_offset;

    if(cam_pitch > 89.0f)
        cam_pitch = 89.0f;
    if(cam_pitch < -89.0f)
        cam_pitch = -89.0f;

    glm::vec3 front;
    front.x = cos(PI180 * cam_pitch) * cos(PI180 * cam_yaw);
    front.y = sin(PI180 * cam_pitch);
    front.z = cos(PI180 * cam_pitch) * sin(PI180 * cam_yaw);

    cam_front = glm::normalize(front);
}

void Camera::updateCameraVectors()
{
    // not implemented
}
