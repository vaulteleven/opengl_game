#include "shader.h"

#include<GL/glew.h>
#include<glm/glm.hpp>

#include<iostream>
#include<fstream>
#include<string>
#include<sstream>

Shader::Shader(const GLchar* vertexPath, 
        const GLchar* fragmentPath, 
        const GLchar* geometryPath = nullptr)
{
    std::string vertexCode;
    std::string fragmentCode;
    std::string geometryCode;

    std::ifstream vShaderFile;
    std::ifstream fShaderFile;
    std::ifstream gShaderFile;

    vShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
    fShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);
    gShaderFile.exceptions (std::ifstream::failbit | std::ifstream::badbit);

    try
    {
        vShaderFile.open(vertexPath);
        fShaderFile.open(fragmentPath);
        std::stringstream vShaderStream, fShaderStream;

        vShaderStream << vShaderFile.rdbuf();
        fShaderStream << fShaderFile.rdbuf();

        vShaderFile.close();
        fShaderFile.close();

        vertexCode = vShaderStream.str();
        fragmentCode = fShaderStream.str();

        if(geometryPath != nullptr)
        {
            gShaderFile.open(geometryPath);
            std::stringstream gShaderStream;
            gShaderStream << gShaderFile.rdbuf();
            gShaderFile.close();
            geometryCode = gShaderStream.str();
        }
    }
    catch(std::ifstream::failure e)
    {
        std::cout << "error shader_file not read" << std::endl;
    }

    const GLchar* vShaderCode = vertexCode.c_str();
    const GLchar* fShaderCode = fragmentCode.c_str();

    GLuint vertex, fragment;
    GLint success;
    GLchar infoLog[512];

    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderCode, NULL);
    glCompileShader(vertex);
    checkCompileErrors(vertex, "VERTEX");

    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fShaderCode, NULL);
    glCompileShader(fragment);
    checkCompileErrors(fragment, "FRAGMENT");

    GLuint geometry;
    if(geometryPath != nullptr)
    {
        const GLchar* gShaderCode = geometryCode.c_str(); 
        geometry = glCreateShader(GL_GEOMETRY_SHADER);
        glShaderSource(geometry, 1, &gShaderCode, NULL);
        glCompileShader(geometry);
        checkCompileErrors(geometry, "GEOMETRY");
    }

    Shader::ID = glCreateProgram();
    glAttachShader(Shader::ID, vertex);
    glAttachShader(Shader::ID, fragment);
    if(geometryPath != nullptr)
        glAttachShader(Shader::ID, geometry);
    glLinkProgram(Shader::ID);
    checkCompileErrors(Shader::ID, "PROGRAM");

    glDeleteShader(vertex);
    glDeleteShader(fragment);
    if(geometryPath != nullptr)
        glDeleteShader(geometry);
}

void Shader::use()
{
    glUseProgram(Shader::ID);
}
void Shader::setBool(const std::string &name, GLboolean value) const
{
    glUniform1i(glGetUniformLocation(Shader::ID, name.c_str()), (int)value);
} 
void Shader::setInt(const std::string &name, GLint value) const
{
    glUniform1i(glGetUniformLocation(Shader::ID, name.c_str()), value);
}
void Shader::setFloat(const std::string &name, GLfloat value) const
{
    glUniform1f(glGetUniformLocation(Shader::ID, name.c_str()), value);
}
void Shader::setVec2(const std::string &name, const glm::vec2 &value) const
{
    glUniform2fv(glGetUniformLocation(Shader::ID, name.c_str()), 1, &value[0]);
}
void Shader::setVec2(const std::string &name, GLfloat x, GLfloat y) const
{
    glUniform2f(glGetUniformLocation(Shader::ID, name.c_str()), x, y);
}
void Shader::setVec3(const std::string &name, const glm::vec3 &value) const
{
    glUniform3fv(glGetUniformLocation(Shader::ID, name.c_str()), 1, &value[0]);
}
void Shader::setVec3(const std::string &name, GLfloat x, GLfloat y, GLfloat z) const
{
    glUniform3f(glGetUniformLocation(Shader::ID, name.c_str()), x, y, z);
}
void Shader::setVec4(const std::string &name, const glm::vec4 &value) const
{
    glUniform4fv(glGetUniformLocation(Shader::ID, name.c_str()), 1, &value[0]);
}
void Shader::setVec4(const std::string &name, GLfloat x, GLfloat y, GLfloat z, GLfloat w)
{
    glUniform4f(glGetUniformLocation(Shader::ID, name.c_str()), x, y, z, w);
}
void Shader::setMat2(const std::string &name, const glm::mat2 &mat) const
{
    glUniformMatrix2fv(glGetUniformLocation(Shader::ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}
void Shader::setMat3(const std::string &name, const glm::mat3 &mat) const
{
    glUniformMatrix3fv(glGetUniformLocation(Shader::ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}
void Shader::setMat4(const std::string &name, const glm::mat4 &mat) const
{
    glUniformMatrix4fv(glGetUniformLocation(Shader::ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
}

GLuint Shader::getID()
{
    return Shader::ID;
}

void Shader::setID(GLuint ID)
{
    Shader::ID = ID;
}

void Shader::checkCompileErrors(GLuint shader, std::string type)
{
    GLint success;
    GLchar infoLog[1024];
    if(type != "PROGRAM")
    {
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if(!success)
        {
            glGetShaderInfoLog(shader, 1024, NULL, infoLog);
            std::cout << "error compiling shader type: " << type << 
                "\n" << infoLog << "\n" << "-------------------" << std::endl;
        }
    }
    else
    {
        glGetProgramiv(shader, GL_LINK_STATUS, &success);
        if(!success)
        {
            glGetProgramInfoLog(shader, 1024, NULL, infoLog);
            std::cout << "error linking shader program type: " << type << 
                "\n" << infoLog << "\n" << "-------------------" << std::endl;
        }

    }
}
