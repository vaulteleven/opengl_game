#version 330 core

out vec4 color;
in vec3 Normal;
in vec3 Fragment_Position;

uniform vec3 object_color;
uniform vec3 light_color;
uniform vec3 light_position;
uniform vec3 camera_position;

void main()
{
    // ambient
    float ambient_strength = 0.1f;
    vec3 ambient = ambient_strength * light_color;

    // diffuse
    vec3 norm = normalize(Normal);
    vec3 light_direction = normalize(light_position - Fragment_Position);
    float diff = max(dot(norm, light_direction), 0.0);
    vec3 diffuse = diff * light_color;
    
    // specular
    float specular_strength = 0.1f;
    vec3 camera_direction = (camera_position - Fragment_Position);
    vec3 reflect_direction = reflect(-light_direction, norm);
    float spec = pow(max(dot(camera_direction, reflect_direction), 0.0), 1);
    vec3 specular = specular_strength * spec * light_color;

    vec3 result = (ambient + diffuse + specular) * object_color;
    color = vec4(result, 1.0f);
}
